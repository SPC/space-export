job("deployFatJar") {
    startOn {
        gitPush {
            anyBranchMatching {
                +"main"
            }
        }
    }
    container("gradle:8.1.1-jdk11") {
        kotlinScript { api ->
            api.gradle("shadowJar")
        }
        fileArtifacts {
            repository = FileRepository("files")
            localPath = "build/libs/space-document-extractor.jar"
            remotePath = "tools/space-document-extractor.jar"
        }
    }
}