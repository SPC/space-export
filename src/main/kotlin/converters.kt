package center.sciprog.space.documentextractor

import java.nio.file.Path
import java.nio.file.StandardOpenOption
import kotlin.io.path.*

internal fun prepareScripts(outputPath: Path): Path {
    val scriptPath = outputPath.resolveSibling("@scripts").resolve("links-to-html.lua")
    if (!scriptPath.exists()) {
        scriptPath.parent.createDirectories()
        scriptPath.writeText(
            {}.javaClass.getResource("/links-to-html.lua")!!.readText(),
            Charsets.UTF_8,
            StandardOpenOption.CREATE
        )
    }
    return scriptPath
}


/**
 * Convert a directory of markdown files to docx, copying other files as is.
 */
@OptIn(ExperimentalPathApi::class)
fun convertToHtml(inputPath: Path, outputPath: Path, indexFileName: String = "index") {
    val scriptPath = prepareScripts(outputPath)
    inputPath.copyToRecursively(outputPath, followLinks = false) { source: Path, target: Path ->
        if (source.isRegularFile() && source.extension == "md") {

            val htmlFileName = source.fileName.nameWithoutExtension.let {
                if (it == indexFileName) "index" else it
            }

            val htmlPath = target.parent.resolve("$htmlFileName.html")

            ProcessBuilder(
                "pandoc",
                "--standalone",
                "--mathjax",
                "--metadata=title: ${source.nameWithoutExtension}",
                "--from=markdown",
                "--to=html5",
                "--lua-filter=${scriptPath.absolute()}",
                "--output=${htmlPath.absolute()}",
                "${source.absolute()}",
            ).also {
                logger.info("Running pandoc: ${it.command().joinToString(separator = " ")}")
            }.directory(source.parent.toFile()).inheritIO().start().waitFor()


            CopyActionResult.CONTINUE
        } else {
            source.copyToIgnoringExistingDirectory(target, false)
        }
    }
}

/**
 * Convert a directory of markdown files to docx files, ignoring other files
 */
@OptIn(ExperimentalPathApi::class)
fun convertToDocX(inputPath: Path, outputPath: Path) {
    inputPath.copyToRecursively(outputPath, followLinks = false) { source: Path, target: Path ->
        if (source.isRegularFile() && source.extension == "md") {

            val docxPath = target.parent.resolve(source.fileName.nameWithoutExtension + ".docx")

            ProcessBuilder(
                "pandoc",
                "--standalone",
                "--from=markdown",
                "--to=docx",
                "--output=${docxPath.absolute()}",
                "${source.absolute()}",
            ).also {
                logger.info("Running pandoc: ${it.command().joinToString(separator = " ")}")
            }.directory(source.parent.toFile()).inheritIO().start().waitFor()
            CopyActionResult.CONTINUE
        } else {
            source.copyToIgnoringExistingDirectory(target, false)
        }
    }
}


//    ZipOutputStream(zipFileName.outputStream().buffered()).use { zipStream ->
//        outputPath.walk().forEach { file ->
//            val zipEntryPath = file.absolute().relativize(inputPath.absolute())
//            val entry = ZipEntry("$zipEntryPath${(if (file.isDirectory()) "/" else "")}")
//            zipStream.putNextEntry(entry)
//            if (file.isRegularFile()) {
//                file.inputStream().copyTo(zipStream)
//            }
//        }
//    }