package center.sciprog.space.documentextractor

import kotlinx.datetime.Instant
import space.jetbrains.api.runtime.SpaceClient
import space.jetbrains.api.runtime.resources.chats
import space.jetbrains.api.runtime.types.*
import java.io.BufferedWriter
import java.nio.file.Path
import java.nio.file.StandardOpenOption
import kotlin.io.path.createDirectories
import kotlin.io.path.outputStream

private suspend fun SpaceClient.writeMessages(
    parentDirectory: Path,
    writer: BufferedWriter,
    id: ChannelIdentifier,
    prefix: String = "",
) {
    var readDateTime: Instant? = null
    var read: Int

    val attachmentsDirectory = parentDirectory.resolve("attachments")
    attachmentsDirectory.createDirectories()

    //reading messages in batches
    do {
        val result: GetMessagesResponse = chats.messages.getChannelMessages(
            channel = id,
            sorting = MessagesSorting.FromOldestToNewest,
            startFromDate = readDateTime,
            batchSize = 50
        ) {
            nextStartFromDate()
            messages {
                author {
                    name()
                }
                text()
                created()
                attachments()
                thread {
                    content()
                }
            }
        }

        result.messages.forEach { message ->
            writer.appendLine(
                """
                |* **(${message.created}) ${message.author.name}:**
                |${message.text}
                """.replaceIndentByMargin(prefix)
            )

            message.attachments?.map { it.details }?.let { attachments ->
                attachments.forEach { attachment: Attachment? ->
                    when (attachment) {
                        is FileAttachment -> {
                            val fileId = attachment.id
                            val name = "${attachment.id}-${attachment.filename}"
                            val file = attachmentsDirectory.resolve(name)
                            extractAttachment(file, fileId)
                            writer.appendLine("*Attachment*: [$name](attachments/$name)\n")
                        }

                        is ImageAttachment -> {
                            val fileId = attachment.id
                            val name = attachment.name?.let { "${attachment.id}-${attachment.name}"} ?: fileId
                            val file = attachmentsDirectory.resolve(name)
                            extractAttachment(file, fileId)
                            writer.appendLine("*Attachment*: [$name](attachments/$name)\n")
                        }

                        is VideoAttachment -> {
                            val fileId = attachment.id
                            val name = attachment.name?.let { "${attachment.id}-${attachment.name}"} ?: fileId
                            val file = attachmentsDirectory.resolve(name)
                            extractAttachment(file, fileId)
                            writer.appendLine("*Attachment*: [$name](attachments/$name)\n")
                        }
                    }
                }
            }

            message.thread?.content?.let {
                if (it is M2ChannelContentThread) {
                    writeMessages(parentDirectory, writer, ChannelIdentifier.Thread(it.record.id), "  ")
                }
            }
        }

        read = result.messages.count()
        readDateTime = result.nextStartFromDate

    } while (read == 50)
}


suspend fun SpaceClient.extractMessages(
    id: ChannelIdentifier,
    parentDirectory: Path,
) {
    val channel = chats.channels.getChannel(id){
        content {
            member {
                name()
                username()
            }
            name()
        }
        contact{
            key()
        }
        totalMessages()
    }

    if (channel.totalMessages == 0){
        logger.debug("Channel with {} is empty", id)
        return
    }

    val name = when(val ext = channel.content){
        is M2SharedChannelContent -> ext.name
        is M2ChannelContentMember -> ext.member.username
        else -> channel.contact.key
    }.replace(fileNameRegex, "_")

    val chatDirectory = parentDirectory.resolve(name)

    val file = chatDirectory.resolve("$name.md")

    logger.info("Extracting messages from channel $id to $file")

    file.parent.createDirectories()

    file.outputStream(
        StandardOpenOption.CREATE,
        StandardOpenOption.TRUNCATE_EXISTING,
        StandardOpenOption.WRITE
    ).bufferedWriter().use { out ->
        writeMessages(chatDirectory, out, id)
    }
}