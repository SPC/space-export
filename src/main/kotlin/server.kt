package center.sciprog.space.documentextractor
//
//import io.ktor.http.HttpStatusCode
//import io.ktor.server.application.Application
//import io.ktor.server.application.call
//import io.ktor.server.cio.CIO
//import io.ktor.server.engine.embeddedServer
//import io.ktor.server.request.*
//import io.ktor.server.response.respond
//import io.ktor.server.routing.*
//import space.jetbrains.api.runtime.SpaceAppInstance
//import space.jetbrains.api.runtime.SpaceAuth
//import space.jetbrains.api.runtime.SpaceClient
//import space.jetbrains.api.runtime.helpers.readPayload
//import space.jetbrains.api.runtime.helpers.verifyWithPublicKey
//import space.jetbrains.api.runtime.ktorClientForSpace
//import space.jetbrains.api.runtime.types.ListCommandsPayload
//import space.jetbrains.api.runtime.types.MessagePayload
//
//fun Application.configureRouting(spaceClient: SpaceClient) {
//    val appInstance = SpaceAppInstance(
//        environment.config.property("space.clientId"),
//
//        clientSecret ?: System.getProperty("space.clientSecret"),
//        spaceUrl
//    )
//
//
//    val spaceClient: SpaceClient = SpaceClient(
//        ktorClientForSpace(io.ktor.client.engine.cio.CIO),
//        appInstance,
//        SpaceAuth.ClientCredentials()
//    )
//
//    routing {
//        post("api/space") {
//            // read request body
//            val body = call.receiveText()
//
//            // read headers required for Space verification
//            val signature = call.request.header("X-Space-Public-Key-Signature")
//            val timestamp = call.request.header("X-Space-Timestamp")?.toLongOrNull()
//            // verifyWithPublicKey gets a key from Space, uses it to generate message hash
//            // and compares the generated hash to the hash in a message
//            if (signature.isNullOrBlank() || timestamp == null || !spaceClient.verifyWithPublicKey(
//                    body, timestamp, signature
//                )
//            ) {
//                call.respond(HttpStatusCode.Unauthorized)
//                return@post
//            }
//
//            // analyze the message payload
//            // MessagePayload = user sends a command
//            // ListCommandsPayload = user types a slash or a char
//            when (val payload = readPayload(body)) {
//                is MessagePayload -> {
//                    runHelpCommand(payload)
//                    call.respond(HttpStatusCode.OK, "")
//                }
//
//                is ListCommandsPayload -> {
//
//                }
//            }
//        }
//    }
//}
//
//fun main() {
//    embeddedServer(CIO, port = 8080) {
//        val
//
//        configureRouting()
//    }.start(wait = true)
//}