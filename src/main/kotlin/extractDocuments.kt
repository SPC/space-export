package center.sciprog.space.documentextractor

import io.ktor.client.request.header
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.client.statement.readBytes
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import space.jetbrains.api.runtime.Batch
import space.jetbrains.api.runtime.SpaceClient
import space.jetbrains.api.runtime.resources.projects
import space.jetbrains.api.runtime.types.*
import java.nio.file.Path
import kotlin.io.path.createDirectories
import kotlin.io.path.writeBytes
import kotlin.io.path.writeText

internal val logger by lazy { LoggerFactory.getLogger("space-extractor") }

/**
 * Extract single attachment image
 */
internal suspend fun SpaceClient.extractAttachment(
    imageFile: Path,
    imageId: String,
) {
    logger.info("Downloading attachment file to $imageFile")
    val response = ktorClient.request {
        url("${server.serverUrl}/d/$imageId")
        method = HttpMethod.Get
        header(HttpHeaders.Authorization, "Bearer ${token().accessToken}")
    }
    imageFile.writeBytes(response.readBytes())
}

/**
 * Extract single file
 */
internal suspend fun SpaceClient.extractFile(
    documentFile: Path,
    documentId: String,
) {
    //https://mipt-npm.jetbrains.space/drive/files/3qe9i43qtPq2
    logger.info("Downloading document file to $documentFile")
    val response = ktorClient.request {
        url("${server.serverUrl}/drive/files/$documentId")
        method = HttpMethod.Get
        header(HttpHeaders.Authorization, "Bearer ${token().accessToken}")
    }
    documentFile.writeBytes(response.readBytes())
}

private val imageRegex = """!\[(?<alt>.*)]\(/d/(?<id>.*)\?f=0( "(?<name>.*)")?\)""".toRegex()
internal val fileNameRegex = "\\W+".toRegex()

/**
 * Download single Space document
 */
internal suspend fun SpaceClient.downloadDocument(
    directory: Path,
    document: Document,
) = coroutineScope {
    when (val body = document.body) {
        is FileDocumentHttpBody -> {
            launch(Dispatchers.IO) {
                val filePath = try {
                    directory.resolve(document.title)
                } catch (ex: Exception) {
                    directory.resolve(document.id)
                }
                extractFile(filePath, document.id)
            }
        }

        is TextDocumentHttpBody -> {
            val markdownFilePath = try {
                directory.resolve(document.title + ".md")
            } catch (ex: Exception) {
                directory.resolve(document.id + ".md")
            }
            when (val content = body.docContent) {
                is MdTextDocumentContent -> {
                    val imageDirectory = directory.resolve("images")
                    imageDirectory.createDirectories()
                    val newText = content.markdown.replace(imageRegex) {
                        val id = it.groups["id"]?.value ?: error("Unexpected reference format: ${it.value}")
                        val alt = it.groups["alt"]?.value?.ifBlank { null }
                        val name = it.groups["name"]?.value?.ifBlank { null }
                        val imageName = (name?.let { "$id-$name" } ?: alt?.let { "$id-$alt" } ?: id)
                            .replace(fileNameRegex, "_")
                        val imageFile = imageDirectory.resolve(imageName)
                        logger.info("Downloading image $id as $imageFile")
                        launch(Dispatchers.IO) {
                            extractAttachment(imageFile, id)
                        }
                        "![${alt ?: ""}](images/$imageName)"
                    }
                    markdownFilePath.writeText(newText, Charsets.UTF_8)
                }

                else -> {
                    logger.error("Rich text documents are unsupported (${document.title})")
                }
            }

        }

        else -> {
            LoggerFactory.getLogger("space-extractor")
                .warn("Can't extract document ${document.title} with type ${document.bodyType}")
        }
    }
}

/**
 * Download all documents and subfolders in a folder
 */
internal suspend fun SpaceClient.downloadDocumentFolder(
    directory: Path,
    projectId: ProjectIdentifier,
    folderId: FolderIdentifier,
) {
    directory.createDirectories()
    logger.info("Processing folder ${folderId.compactId} to $directory")
    val documents = projects.documents.folders.documents.listDocumentsInFolder(projectId, folderId) {
        id()
    }
    documents.data.forEach {
        val document = projects.documents.getDocument(projectId, it.id) {
            body()
            bodyType()
            title()
            id()
        }
        val bodyInfo = document.body
        if ((bodyInfo is TextDocumentHttpBody) && bodyInfo.docContent !is MdTextDocumentContent) {
            //make a conversion to markdown before downloading
            logger.info("Converting document ${document.title} in ${folderId.compactId} to markdown format")
            val convertedDocument = projects.documents.updateDocument(
                project = projectId,
                documentId = it.id,
                updateIn = TextDocumentBodyConvertTypeIn(
                    type = DraftDocumentType.MARKDOWN
                )
            ) {
                body()
                bodyType()
                title()
                id()
            }
            downloadDocument(directory, convertedDocument)
        } else {
            downloadDocument(directory, document)
        }
    }

    val subFolders: Batch<DocumentFolder> = projects.documents.folders.subfolders.listSubfolders(projectId, folderId)
    subFolders.data.forEach {
        val subPath = directory.resolve(it.name)
        downloadDocumentFolder(subPath, projectId, FolderIdentifier.Id(it.id))
    }
}

/**
 * Download all documents in a project or a folder with given [rootFolder] and postprocess files
 *
 * @param directory target directory
 */
suspend fun SpaceClient.downloadAndProcessDocumentsInProject(
    directory: Path,
    projectId: ProjectIdentifier,
    rootFolder: FolderIdentifier = FolderIdentifier.Root,
) = withContext(Dispatchers.IO) {
    logger.info("Processing project ${projectId.compactId} to $directory")
    downloadDocumentFolder(directory, projectId, rootFolder)
//    processMarkdownInDirectory(directory)
}