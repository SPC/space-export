package center.sciprog.space.documentextractor

import space.jetbrains.api.runtime.SpaceClient
import space.jetbrains.api.runtime.resources.projects
import space.jetbrains.api.runtime.types.ProjectIdentifier
import java.nio.file.Path

/**
 * Clone a single repository to a [parentDirectory]
 */
private fun cloneRepo(
    parentDirectory: Path,
    url: String,
) {
    logger.info("Cloning $url to $parentDirectory")

    ProcessBuilder("git", "clone", url)
        .directory(parentDirectory.toAbsolutePath().toFile())
        .inheritIO()
        .start()
        .waitFor()

//    Git.cloneRepository()
//        .setURI(url)
//        .setDirectory(parentDirectory.resolve(name).toFile())
//        .call()
}

/**
 * Extract all repos in the project into a [parentDirectory]
 */
suspend fun SpaceClient.extractRepos(
    parentDirectory: Path,
    projectId: ProjectIdentifier,
) {
    val repos = projects.getProject(
        project = projectId
    ) {
        repos()
    }.repos

    repos.forEach { repo ->
        try {
            val url = projects.repositories.url(
                project = projectId,
                repository = repo.name
            ).sshUrl ?: error("Could not resolve sshUrl for ${repo.name}")
            cloneRepo(parentDirectory = parentDirectory, url)
        } catch (ex: Exception) {
            logger.error("Failed ", ex)
        }
    }
}