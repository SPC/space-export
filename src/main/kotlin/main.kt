@file:OptIn(ExperimentalCli::class)

package center.sciprog.space.documentextractor

import io.ktor.client.engine.cio.CIO
import kotlinx.cli.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import space.jetbrains.api.runtime.*
import space.jetbrains.api.runtime.resources.chats
import space.jetbrains.api.runtime.resources.projects
import space.jetbrains.api.runtime.resources.teamDirectory
import space.jetbrains.api.runtime.types.*
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.createDirectories
import kotlin.io.path.div

private abstract class ExtractCommand(name: String, description: String) : Subcommand(name, description) {

    val url by argument(
        ArgType.String,
        description = """
            Root IRL of the space Url like `https://spc.jetbrains.space`.
            OR 
            Url of a specific page like: `https://spc.jetbrains.space/im/user/TestAccount`.
        """.trimIndent()
    )

    val clientId by option(
        ArgType.String,
        description = "Space application client ID (if not defined, use environment value 'space.clientId')"
    )

    val clientSecret by option(
        ArgType.String,
        description = "Space application client secret (if not defined, use environment value 'space.clientSecret')"
    )
}

private class ExtractDocumentsCommand : ExtractCommand("docs", "Extract documents") {

    val path: String? by option(
        ArgType.String,
        description = "Target directory. Default is './documents/<id>'."
    )

    val html by option(
        ArgType.Boolean,
        description = "Convert Markdown to HTML via pandoc"
    ).default(false)

    val htmlPath by option(
        ArgType.String,
        description = "Path for html output. Default is './documents/@html/<id>"
    )

    val docx by option(
        ArgType.Boolean,
        description = "Convert Markdown to DOCX via pandoc"
    ).default(false)

    val docxPath by option(
        ArgType.String,
        description = "Path for docx output. Default is './documents/@docx/<id>"
    )

    val exportRepos by option(
        ArgType.Boolean,
        description = "Export all repositories in the project."
    ).default(false)


    override fun execute() {
        val urlMatch = urlRegex.matchEntire(url) ?: error("Url $url does not match space document url pattern")

        val spaceUrl = urlMatch.groups["spaceUrl"]?.value ?: error("Space Url token not recognized")

        val project = urlMatch.groups["projectName"]?.value ?: error("Project name token not recognized")

        val folderId: String? = urlMatch.groups["folderId"]?.value

        val markdownPath: Path = path?.let { Path(it) } ?: Path("markdown/${folderId ?: project}")

        Files.createDirectories(markdownPath)

        val appInstance = SpaceAppInstance(
            clientId ?: System.getProperty("space.clientId"),
            clientSecret ?: System.getProperty("space.clientSecret"),
            spaceUrl
        )


        val spaceClient: SpaceClient = SpaceClient(
            ktorClientForSpace(CIO),
            appInstance,
            SpaceAuth.ClientCredentials()
        )
        runBlocking {
            println("Processing project \"${spaceClient.projects.getProject(ProjectIdentifier.Key(project)).name}\"")
            spaceClient.downloadAndProcessDocumentsInProject(
                markdownPath,
                ProjectIdentifier.Key(project),
                folderId?.let { FolderIdentifier.Id(it) } ?: FolderIdentifier.Root
            )
            if (html) {
                val htmlTargetPath = path?.let { Path(it) }?.resolve(htmlPath ?: "html")
                    ?: Path(htmlPath ?: "html/${folderId ?: project}")
                htmlTargetPath.createDirectories()
                convertToHtml(markdownPath, htmlTargetPath)
            }
            if (docx) {
                val docxTargetPath = path?.let { Path(it) }?.resolve(docxPath ?: "docx")
                    ?: Path(docxPath ?: "docx/${folderId ?: project}")
                docxTargetPath.createDirectories()
                convertToDocX(markdownPath, docxTargetPath)
            }
        }
    }

    companion object {
        private val urlRegex =
            """(?<spaceUrl>https?:\/\/[^\/]*)\/p\/(?<projectName>[^\/]*)(\/.*-(?<folderId>.*)${'$'})?""".toRegex()
    }
}

private class ExtractRepositoriesCommand : ExtractCommand("repos", "Extract repositories") {

    val path: String by option(
        ArgType.String,
        description = "Target directory."
    ).default("./repositories")

    override fun execute() {
        val urlMatch = urlRegex.matchEntire(url) ?: error("Url $url does not match space document url pattern")

        val spaceUrl = urlMatch.groups["spaceUrl"]?.value ?: error("Space Url token not recognized")

        val project = urlMatch.groups["projectName"]?.value ?: error("Project name token not recognized")

        val repoPath: Path = Path(path)

        Files.createDirectories(repoPath)

        val appInstance = SpaceAppInstance(
            clientId ?: System.getProperty("space.clientId"),
            clientSecret ?: System.getProperty("space.clientSecret"),
            spaceUrl
        )

        val spaceClient: SpaceClient = SpaceClient(
            ktorClientForSpace(CIO),
            appInstance,
            SpaceAuth.ClientCredentials()
        )

        runBlocking {
            val key = ProjectIdentifier.Key(project)
            logger.info("Extracting repositories from project \"${spaceClient.projects.getProject(key).name}\"")
            spaceClient.extractRepos(
                repoPath,
                key,
            )
        }
    }

    companion object {
        private val urlRegex =
            """(?<spaceUrl>https?:\/\/[^\/]*)\/p\/(?<projectName>[^\/]*)\/?""".toRegex()
    }

}

private class ExtractChannelsCommand : ExtractCommand("channels", "Extract all messages from a channels") {

    val path: String by option(
        ArgType.String,
        description = "Target directory."
    ).default("./channels")

    override fun execute() {
        val urlMatch = urlRegex.matchEntire(url) ?: error("Url $url does not match space document url pattern")

        val spaceUrl = urlMatch.groups["spaceUrl"]?.value ?: error("Space Url token not recognized")

        val channelId = urlMatch.groups["chatId"]?.value

        val appInstance = SpaceAppInstance(
            clientId ?: System.getProperty("space.clientId"),
            clientSecret ?: System.getProperty("space.clientSecret"),
            spaceUrl
        )

        val spaceClient: SpaceClient = SpaceClient(
            ktorClientForSpace(CIO),
            appInstance,
            SpaceAuth.ClientCredentials()
        )

        runBlocking(Dispatchers.IO) {
            if (channelId == null) {
                spaceClient.chats.channels.listAllChannels(query = "").data.forEach { channel ->
                    launch {
                        try {
                            spaceClient.extractMessages(ChannelIdentifier.Id(channel.channelId), Path(path))
                        } catch (ex: Exception) {
                            logger.error("Failed to download ${channel.name}", ex)
                        }
                    }
                }
            } else {
                spaceClient.extractMessages(ChannelIdentifier.Id(channelId), Path(path))
            }
        }
    }

    companion object {
        private val urlRegex =
            """(?<spaceUrl>https?:\/\/[^\/]*)(\/im\/group\/(?<chatId>.*))?""".toRegex()
    }
}

private class ExtractDirectCommand : Subcommand("direct", "Extract direct messages") {

    val url by argument(
        ArgType.String,
        description = """
            Root IRL of the space Url like `https://spc.jetbrains.space`.
            OR 
            Url of a specific conversation like: `https://spc.jetbrains.space/im/user/TestAccount`.
        """.trimIndent()
    )

    val token by option(
        ArgType.String,
        description = "A permanent token. Must have `View direct messages`, `View messages` and `View profile` access."
    ).required()

    val path: String by option(
        ArgType.String,
        description = "Target directory."
    ).default("./messages")

    override fun execute() {
        val urlMatch = urlRegex.matchEntire(url) ?: error("Url $url does not match space document url pattern")

        val spaceUrl = urlMatch.groups["spaceUrl"]?.value ?: error("Space Url token not recognized")

        val profileName = urlMatch.groups["profileName"]?.value

        val spaceClient = SpaceClient(
            ktorClient = ktorClientForSpace(CIO),
            serverUrl = spaceUrl,
            token = token//"eyJhbGciOiJSUzUxMiJ9.eyJzdWIiOiJOcHJJcjFNUU5layIsImF1ZCI6ImNpcmNsZXQtd2ViLXVpIiwib3JnRG9tYWluIjoic3BjIiwibmFtZSI6ImFsdGF2aXIiLCJpc3MiOiJodHRwczpcL1wvc3BjLmpldGJyYWlucy5zcGFjZSIsInBlcm1fdG9rZW4iOiIycDRtSW4zZ281SUciLCJwcmluY2lwYWxfdHlwZSI6IlVTRVIiLCJpYXQiOjE2OTM1Nzg2NDd9.anDeWjBctaC4FWdjDvGS7KqWaScrE1hC2CHzLSd3K_g-xcxtcqMnls8AzjCWSTeyAG5bWsXak73t2JiUqf6LjQnUkXidLhS33Odq5defl-a2QABxWNehCHQJlDQmFX20Hh3WUCqzIxpON_1eAtN5iWXnsxdMryzR7MbwsyLTdhM"
        )

        runBlocking {
            if (profileName == null) {
                spaceClient.teamDirectory.profiles.getAllProfiles(
                    batchInfo = BatchInfo(
                        offset = null,
                        batchSize = 1000
                    )
                ) {
                    id()
                }.data.forEach {
                    spaceClient.extractMessages(ChannelIdentifier.Profile(ProfileIdentifier.Id(it.id)), Path(path))
                }
            } else {
                spaceClient.extractMessages(
                    ChannelIdentifier.Profile(ProfileIdentifier.Username(profileName)),
                    Path(path)
                )
            }
        }
    }

    companion object {
        private val urlRegex =
            """(?<spaceUrl>https?:\/\/[^\/]*)(\/im\/user\/(?<profileName>.*))?""".toRegex()
    }
}

private class ExtractProjectCommand : ExtractCommand("project", "Extract all data from a project") {

    val path: String by option(
        ArgType.String,
        description = "Target directory."
    ).default(".")

    override fun execute() {
        val urlMatch = urlRegex.matchEntire(url) ?: error("Url $url does not match space document url pattern")

        val spaceUrl = urlMatch.groups["spaceUrl"]?.value ?: error("Space Url token not recognized")

        val projectKey = urlMatch.groups["projectKey"]?.value


        val appInstance = SpaceAppInstance(
            clientId ?: System.getProperty("space.clientId"),
            clientSecret ?: System.getProperty("space.clientSecret"),
            spaceUrl
        )

        val spaceClient: SpaceClient = SpaceClient(
            ktorClientForSpace(CIO),
            appInstance,
            SpaceAuth.ClientCredentials()
        )

        fun CoroutineScope.downloadProject(project: PR_Project) {
            val key = ProjectIdentifier.Key(project.key.key)
            val projectPath = Path(path) / "projects" / project.name
            logger.info("Extracting everything from project \"${project.name}\"")
            val documentsPath = projectPath / "documents"
            Files.createDirectories(documentsPath)

            val repoPath: Path = projectPath / "repositories"

            Files.createDirectories(repoPath)
            launch {
                spaceClient.extractRepos(
                    repoPath,
                    key,
                )
            }
            launch {
                spaceClient.downloadAndProcessDocumentsInProject(
                    documentsPath,
                    key,
                    FolderIdentifier.Root
                )
            }
        }


        runBlocking(Dispatchers.IO) {
            if (projectKey == null) {
                logger.info("Extracting everything from all available projects")
                spaceClient.projects.getAllProjects().data.forEach {
                    downloadProject(it)
                }

            } else {
                downloadProject(spaceClient.projects.getProject(ProjectIdentifier.Key(projectKey)))
            }
        }
    }

    companion object {
        private val urlRegex =
            """(?<spaceUrl>https?:\/\/[^\/]*)(\/p\/(?<projectKey>[^\/]*))?\/?""".toRegex()
    }

}


fun main(args: Array<String>) {
    val parser = ArgParser("space-export")

    parser.subcommands(
        ExtractDocumentsCommand(),
        ExtractRepositoriesCommand(),
        ExtractProjectCommand(),
        ExtractChannelsCommand(),
        ExtractDirectCommand()
    )

    parser.parse(args)
}