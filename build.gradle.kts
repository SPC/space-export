import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    kotlin("jvm") version "1.8.22"
    application
    id("com.github.johnrengelman.shadow") version "8.1.1"
}

group = "center.sciprog"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://maven.pkg.jetbrains.space/public/p/space/maven")
}

val ktorVersion = "2.3.4"

dependencies {
    implementation("org.jetbrains:space-sdk-jvm:167818-beta")
    implementation("io.ktor:ktor-client-cio-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-core-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-cio-jvm:$ktorVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-cli:0.3.5")
    implementation("ch.qos.logback:logback-classic:1.4.8")
    implementation("org.eclipse.jgit:org.eclipse.jgit:6.6.0.202305301015-r")
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(11)
}

application {
    mainClass.set("center.sciprog.space.documentextractor.MainKt")
}

tasks.withType<ShadowJar>{
    archiveClassifier.set("")
    archiveVersion.set("")
}